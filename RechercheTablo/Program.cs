﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RechercheTablo
{
    public class Program
    {
        public static int Saisie_nombre_cases() //Retourne le nombre de cases positive ou nulle
        {
            int nombre_de_valeur;
            string nombre_de_valeur_as_string;
            do
            {
                Console.WriteLine("Saisissez le nombre de valeur dans le tableau");
                nombre_de_valeur_as_string = Console.ReadLine();
                int.TryParse(nombre_de_valeur_as_string, out nombre_de_valeur);
                if (nombre_de_valeur == 0)
                    Console.WriteLine("Veuillez saisir une valeur supérieure a 0");
            } while (nombre_de_valeur <= 0); //tant que la saisie est inferieure ou egale a 0
            return nombre_de_valeur;
        }
        public static int Saisie_Valeur_recherchee()
        {
            int valeur_recherchee;
            string valeur_recherchee_as_string;
            do
            {
                Console.WriteLine("Saisissez une valeur : ");
                valeur_recherchee_as_string = Console.ReadLine();
                int.TryParse(valeur_recherchee_as_string, out valeur_recherchee);
                if (valeur_recherchee < 0)
                    Console.WriteLine("Veuillez saisir une valeur positive ou nulle");
            } while (valeur_recherchee < 0);
            return valeur_recherchee;
        }
        public static int[] Initialisation_Tableau(int nombre_de_cases) //retourne un tableau de type int
        {
            int valeur_saisie;
            int[] tableau_de_valeurs = new int[nombre_de_cases];
            string valeur_saisie_as_string;
            for (int increment = 0; increment < nombre_de_cases; increment++) 
            {/*Teste a chaque cases si les valeurs entrees sont pas nulles ou inferieures a 0*/
                do
                {
                    Console.WriteLine("Entrer la valeur {0} du tableau", increment + 1);
                    valeur_saisie_as_string = Console.ReadLine();
                    int.TryParse(valeur_saisie_as_string, out valeur_saisie);
                    tableau_de_valeurs[increment] = valeur_saisie;
                    if (valeur_saisie < 0)
                        Console.WriteLine("Veuillez saisir une valeur positive ou nulle");
                } while (valeur_saisie < 0);
                
            }
            return tableau_de_valeurs;
        }
        public static bool Recherche_Valeur(int nombre_de_cases, int valeur_recherchee,int [] tableau_de_valeur)
        {
            bool resultat = false;
            int increment = 0;
            int position_valeur_recherche=-1;
            string phrase_conclusion;
            do
            {
                if (tableau_de_valeur[increment] == valeur_recherchee)
                {
                    position_valeur_recherche = increment + 1;
                    resultat = true;
                }
                increment++;
            } while (increment < tableau_de_valeur.Length && position_valeur_recherche == -1);

            if (resultat==true)
            {
                phrase_conclusion = String.Format("{0} est la {1}-eme valeur saisie ", valeur_recherchee, position_valeur_recherche);
                Console.WriteLine(phrase_conclusion);
            }
            else
            {
                phrase_conclusion = String.Format("La valeur {0} n'est pas dans le tableau, sorry :) ", valeur_recherchee);
                Console.WriteLine(phrase_conclusion);
            }
            return resultat;

        }

        static void Main(string[] args)
        {
            int nombre_valeurs_tablo;
            int valeur_recherchee;
            nombre_valeurs_tablo = Saisie_nombre_cases();
            int[] tableau_de_valeurs = new int[nombre_valeurs_tablo];
            tableau_de_valeurs = Initialisation_Tableau(nombre_valeurs_tablo);
            valeur_recherchee = Saisie_Valeur_recherchee();
            Recherche_Valeur(nombre_valeurs_tablo, valeur_recherchee, tableau_de_valeurs);
            Console.ReadKey();
        }
    }
}
