﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using RechercheTablo;

namespace TestsRecherche
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Test_Initialisation_Tableau()
        {
            Assert.IsFalse(Program.Initialisation_Tableau(1).Length == 0);
            Assert.IsNotNull(Program.Initialisation_Tableau(1));
            Assert.AreEqual(4, Program.Initialisation_Tableau(4).Length);
        }
        [TestMethod]
        public void Test_Recherche_Tableau()
        {
            int[] tableau_de_valeurs=Program.Initialisation_Tableau(5);
            Assert.AreEqual(true, Program.Recherche_Valeur(4, 5,tableau_de_valeurs));
            Assert.AreEqual(false, Program.Recherche_Valeur(0, 5,tableau_de_valeurs));
            Assert.AreEqual(false, Program.Recherche_Valeur(6, -1,tableau_de_valeurs));
        }
        [TestMethod]
        public void Test_Saisie_nombre_cases()
        {
            Assert.IsNotNull(Program.Saisie_nombre_cases());
            Assert.IsFalse(Program.Saisie_nombre_cases() <= 0);
        }
        [TestMethod]
        public void Test_Saisie_valeur_recherchee()
        {
            Assert.IsFalse(Program.Saisie_Valeur_recherchee() < 0);
            Assert.IsTrue(Program.Saisie_Valeur_recherchee() >= 0);
        }
    }
}
